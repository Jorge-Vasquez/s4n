package com.s4n.corrientazo;

import com.s4n.corrientazo.exceptions.FueraDelPerimetroException;
import com.s4n.corrientazo.services.DomicilioService;
import com.s4n.corrientazo.services.FilesService;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.s4n.corrientazo.model.Constants.RESOURCES;


class CorrientazoAppTest {

    public static final int LIMITE_DE_PERIMETRO = 10;

    public static final int NUMERO_DE_PEDIDO_POR_ENTREGA = 3;


    @Test
    void appEjecucionExitosa() throws IOException {

        Set<Path> archivos = FilesService.obtenerArchivos("src/test/resources/");
        DomicilioService domicilioService = new DomicilioService(LIMITE_DE_PERIMETRO, NUMERO_DE_PEDIDO_POR_ENTREGA);
        domicilioService.ejecutarRutas(archivos);

        File file = FileUtils.getFile(RESOURCES + "/" + "out01.txt");
        Stream<String> lines = Files.lines(file.toPath());
        List<String> linesStr = lines.collect(Collectors.toList());

        Assertions.assertNotNull(RESOURCES);
        Assertions.assertEquals(5, linesStr.size());
        Assertions.assertEquals("== Reporte de entregas ==", linesStr.get(0));
        Assertions.assertEquals("(-2, 4) dirección NORTE", linesStr.get(1));
        Assertions.assertEquals("(-3, 3) dirección SUR", linesStr.get(2));
        Assertions.assertEquals("(-4, 2) dirección ORIENTE", linesStr.get(3));

        Files.deleteIfExists(Path.of(RESOURCES + "/" + "out01.txt"));
    }


    @Test()
    void appDomicilioFueraDelPerimetro() throws IOException {
        try {
            Set<Path> archivos = FilesService.obtenerArchivos("src/test/resources/exception");
            DomicilioService domicilioService = new DomicilioService(LIMITE_DE_PERIMETRO, NUMERO_DE_PEDIDO_POR_ENTREGA);
            domicilioService.ejecutarRutas(archivos);
        } catch (FueraDelPerimetroException e) {
            Assertions.assertEquals(
                    "Excede numero de cuadras a la redonda, favor validar ruta: AAAAAAAAAAAAAAAAAAAAA para el drone id:02",
                    e.getMessage());
        }
    }

}