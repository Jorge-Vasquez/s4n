package com.s4n.corrientazo.model;

import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
public class Domicilio {

    private String ruta;

    private Drone droneAsignado;

}

