package com.s4n.corrientazo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Position {

    private int ejeX;

    private int ejeY;

    private Orientation direccionActual;

    public Position() {
        this.ejeX = 0;
        this.ejeY = 0;
        this.direccionActual = Orientation.NORTE;
    }

    public Position(int ejeX, int ejeY, Orientation direccionActual) {
        this.ejeX = ejeX;
        this.ejeY = ejeY;
        this.direccionActual = direccionActual;
    }

}
