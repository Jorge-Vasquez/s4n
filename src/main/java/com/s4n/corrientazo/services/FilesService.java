package com.s4n.corrientazo.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.s4n.corrientazo.model.Constants.BARRA_DIAGONAL;
import static com.s4n.corrientazo.model.Constants.INPUT_PREFIX;
import static com.s4n.corrientazo.model.Constants.MENSAJE_REPORTE;
import static com.s4n.corrientazo.model.Constants.OUT;
import static com.s4n.corrientazo.model.Constants.RESOURCES;
import static com.s4n.corrientazo.model.Constants.TXT_EXTENSION;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FilesService {

    public static Set<Path> obtenerArchivos(String dir) throws IOException {
        try (Stream<Path> stream = Files.list(Paths.get(dir))) {
            return stream.filter(path -> !Files.isDirectory(path))
                    .filter(pathStr -> pathStr.getFileName().toString().startsWith(INPUT_PREFIX))
                    .collect(Collectors.toSet());
        }
    }

    public static void escribirReporte(String id, String informacion) throws IOException {
        String pathStr = RESOURCES + BARRA_DIAGONAL + OUT + id + TXT_EXTENSION;
        Path path = Path.of(pathStr);
        if (!Files.exists(path)) {
            new File(pathStr);
            informacion = MENSAJE_REPORTE + System.lineSeparator() + informacion;
            Files.write(path, informacion.getBytes());
        } else {
            informacion = System.lineSeparator() + informacion;
            Files.write(path, informacion.getBytes(), StandardOpenOption.APPEND);
        }
    }

}
