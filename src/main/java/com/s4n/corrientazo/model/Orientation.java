package com.s4n.corrientazo.model;

import lombok.Getter;

@Getter
public enum Orientation {

    NORTE(Constants.NORTE, Constants.ORIENTE, Constants.OCCIDENTE),
    SUR(Constants.SUR, Constants.OCCIDENTE, Constants.ORIENTE),
    ORIENTE(Constants.ORIENTE, Constants.SUR, Constants.NORTE),
    OCCIDENTE(Constants.OCCIDENTE, Constants.NORTE, Constants.SUR);

    private final String nombre;

    private final String derecha;

    private final String izquierda;

    Orientation(String nombre, String derecha, String izquierda) {
        this.nombre = nombre;
        this.derecha = derecha;
        this.izquierda = izquierda;
    }
}
