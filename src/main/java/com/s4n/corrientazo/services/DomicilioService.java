package com.s4n.corrientazo.services;

import com.s4n.corrientazo.exceptions.FueraDelPerimetroException;
import com.s4n.corrientazo.model.Domicilio;
import com.s4n.corrientazo.model.Drone;
import com.s4n.corrientazo.model.Orientation;
import com.s4n.corrientazo.model.Position;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

import static com.s4n.corrientazo.model.Constants.DIRECCION;
import static com.s4n.corrientazo.model.Constants.INDICE;
import static com.s4n.corrientazo.model.Constants.PUNTO;
import static com.s4n.corrientazo.model.Constants.SUFIJO_DRONE_NAME;


public class DomicilioService {

    private final int limiteDePerimetro;

    private final int numeroDePedidoPorEntrega;

    public DomicilioService(int limiteDePerimetro, int numeroDePedidoPorEntrega) {
        this.limiteDePerimetro = limiteDePerimetro;
        this.numeroDePedidoPorEntrega = numeroDePedidoPorEntrega;
    }

    public void ejecutarRutas(Set<Path> archivos) throws IOException {
        for (Path archivo : archivos) {
            ejecutar(archivo);
        }
    }

    public void ejecutar(Path archivo) throws IOException {
        List<Domicilio> domicilios = asignarDroneToRuta(archivo);
        for (Domicilio domicilio : domicilios) {
            validarRuta(domicilios);
            entregar(domicilio);
        }

    }

    private List<Domicilio> asignarDroneToRuta(Path archivo) throws IOException {
        String fileName = archivo.getFileName().toString();
        String droneId = fileName.substring(fileName.indexOf(SUFIJO_DRONE_NAME) + INDICE, fileName.indexOf(PUNTO));

        Drone droneAsignado = new Drone(droneId);
        droneAsignado.setCapacidadDeCarga(this.numeroDePedidoPorEntrega);

        return Files.lines(archivo)
                .filter(StringUtils::isNotBlank)
                .map(ruta -> Domicilio.builder()
                        .droneAsignado(droneAsignado)
                        .ruta(ruta)
                        .build())
                .collect(Collectors.toList());
    }

    private void entregar(Domicilio domicilio) throws IOException {
        validarCarga(domicilio.getDroneAsignado());
        enviar(domicilio);
        reportar(domicilio);
    }

    private void validarCarga(Drone drone) {
        Position positionInicial = new Position(0, 0, Orientation.NORTE);
        if (drone.getCapacidadDeCarga() == 0) {
            drone.setPosition(positionInicial);
        }
    }

    private void enviar(Domicilio domicilio) {
        char[] instructions = domicilio.getRuta().toCharArray();
        for (char instruction : instructions) {
            domicilio.getDroneAsignado().ejecutarInstruccion(instruction);
        }
        domicilio.getDroneAsignado().setCapacidadDeCarga(domicilio.getDroneAsignado().getCapacidadDeCarga() - INDICE);
    }


    private void validarRuta(List<Domicilio> domicilios) {
        Drone drone = new Drone("reconocimento");
        for (Domicilio domicilio : domicilios) {
            Position posicionActual = validarRecorrido(domicilio, drone);
            validarPerimetro(posicionActual, domicilio);
        }
    }

    private Position validarRecorrido(Domicilio domicilio, Drone drone) {
        char[] instructions = domicilio.getRuta().toCharArray();
        for (char instruction : instructions) {
            drone.ejecutarInstruccion(instruction);
        }
        return drone.getPosition();
    }

    private void
    validarPerimetro(Position posicionEsperada, Domicilio domicilio) {
        int xEsperada = Math.abs(posicionEsperada.getEjeX());
        int yEsperada = Math.abs(posicionEsperada.getEjeY());
        if (xEsperada >= this.limiteDePerimetro || yEsperada >= this.limiteDePerimetro) {
            throw new FueraDelPerimetroException(
                    "Excede numero de cuadras a la redonda, favor validar ruta: " + domicilio
                            .getRuta() + " para el drone id:" + domicilio.getDroneAsignado().getId());
        }
    }

    private void reportar(Domicilio domicilio) throws IOException {
        Drone drone = domicilio.getDroneAsignado();
        Position position = drone.getPosition();
        String reporte = "(" + position.getEjeX() + ", " + position.getEjeY() + ") " + DIRECCION +
                position.getDireccionActual();
        FilesService.escribirReporte(drone.getId(), reporte);
    }


}
