package com.s4n.corrientazo;

import com.s4n.corrientazo.services.DomicilioService;
import com.s4n.corrientazo.services.FilesService;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

import static com.s4n.corrientazo.model.Constants.RESOURCES;


public class CorrientazoApp {

    public static void main(String[] args) throws IOException {
        Set<Path> pathSet = FilesService.obtenerArchivos(RESOURCES);
        DomicilioService domicilioService = new DomicilioService(10, 3);
        domicilioService.ejecutarRutas(pathSet);
    }

}
