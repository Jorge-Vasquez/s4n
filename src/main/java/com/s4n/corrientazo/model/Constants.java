package com.s4n.corrientazo.model;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final int INDICE = 1;

    public static final String SUR = "SUR";

    public static final String NORTE = "NORTE";

    public static final String ORIENTE = "ORIENTE";

    public static final String OCCIDENTE = "OCCIDENTE";

    public static final char ADELANTE = 'A';

    public static final char DERECHA = 'D';

    public static final char IZQUIERDA = 'I';

    public static final String SUFIJO_DRONE_NAME = "n";

    public static final String PUNTO = ".";

    public static final String INPUT_PREFIX = "in";

    public static final String RESOURCES = "src/main/resources";

    public static final String BARRA_DIAGONAL = "/";

    public static final String OUT = "out";

    public static final String TXT_EXTENSION = ".txt";

    public static final String DIRECCION = "dirección ";

    public static final String MENSAJE_REPORTE = "== Reporte de entregas ==";

}
