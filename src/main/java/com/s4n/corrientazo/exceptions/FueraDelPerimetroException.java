package com.s4n.corrientazo.exceptions;

public class FueraDelPerimetroException extends RuntimeException {

    public FueraDelPerimetroException(String message) {
        super(message);
    }


}
