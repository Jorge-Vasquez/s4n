package com.s4n.corrientazo.model;

import lombok.Getter;
import lombok.Setter;

import static com.s4n.corrientazo.model.Constants.ADELANTE;
import static com.s4n.corrientazo.model.Constants.DERECHA;
import static com.s4n.corrientazo.model.Constants.IZQUIERDA;

@Setter
@Getter
public class Drone {

    public static final int PASO_DE_AVANCE = 1;

    private String id;

    private Position position;

    private int capacidadDeCarga;

    public Drone(String id) {
        this.id = id;
        this.position = new Position();
    }

    public void ejecutarInstruccion(char instruccion) {
        switch (instruccion) {
            case ADELANTE:
                this.avanzar();
                break;
            case IZQUIERDA:
                girar(IZQUIERDA);
                break;
            case DERECHA:
                girar(DERECHA);
                break;
            default:
                break;
        }
    }

    private void avanzar() {
        String direccionActual = this.position.getDireccionActual().getNombre();
        actualizarPosition(direccionActual);
    }

    private void girar(char direccion) {
        switch (direccion) {
            case IZQUIERDA:
                this.position.setDireccionActual(Orientation.valueOf(position.getDireccionActual().getIzquierda()));
                break;
            case DERECHA:
                this.position.setDireccionActual(Orientation.valueOf(position.getDireccionActual().getDerecha()));
                break;
            default:
                break;
        }
    }

    private void actualizarPosition(String direccionActual) {
        switch (direccionActual) {
            case Constants.NORTE:
                this.position.setEjeY(this.position.getEjeY() + PASO_DE_AVANCE);
                break;
            case Constants.SUR:
                this.position.setEjeY(this.position.getEjeY() - PASO_DE_AVANCE);
                break;
            case Constants.ORIENTE:
                this.position.setEjeX(this.position.getEjeX() + PASO_DE_AVANCE);
                break;
            case Constants.OCCIDENTE:
                this.position.setEjeX(this.position.getEjeX() - PASO_DE_AVANCE);
                break;
            default:
                break;
        }
    }

}
